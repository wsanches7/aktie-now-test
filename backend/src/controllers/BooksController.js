const Book = require('../models/Book');

const list = async (req, res) => {
  try {
    const { name } = req.query;
    const books = !name ? await Book.find() : await Book.find({ name });

    if(!books.length)
      return res.status(404).json({ 'message': 'Nenhum registro encontrado.' });

    return res.status(200).json(books);
  } catch (error) {
    return res.status(500).json({ error });
  }
};

const show = async (req, res) => {
  try {
    const { id } = req.params;

    const book = await Book.findById(id);

    if(!book)
      return res.status(404).json({ 'message': 'Nenhum registro encontrado com este ID.' });

    return res.status(200).json(book);
  } catch (error) {
    return res.status(500).json({ error });
  }
};

const store = async (req, res) => {
  try {
    const {
      name, author, abstract, comments,
    } = req.body;
  
    let book = await Book.findOne({ name });
  
    if (book && book.author === author)
      return res.status(409).json({ error: `Já existe um livro cadastrado com o nome ${name} do mesmo author.` });
  
    book = await Book.create({
      name, author, abstract, comments,
    });
  
    return res.status(201).send(book);
  } catch (error) {
    return res.status(500).json({ error });
  }
};

const update = async (req, res) => {
  try {
    const { id } = req.params;

    const book = await Book.findById(id);

    const name = req.body.name || book.name;
    const author = req.body.author || book.author;
    const abstract = req.body.abstract || book.abstract;
    const comments = req.body.comments || book.comments;

    await Book.updateOne({ _id: book._id }, {
      name, author, abstract, comments,
    });

    const newBook = await Book.findById(id);

    return res.status(200).json(newBook);
  } catch (error) {
    return res.status(500).json({ error });
  }
};

const remove = async (req, res) => {
  try {
    const { id } = req.params;
  
    const { deletedCount } = await Book.deleteOne({ _id: id });

    return res.status(204).json({ deletedCount });
  } catch (error) {
    return res.status(500).json({ error });
  }
};

module.exports = {
  list, store, show, update, remove,
};
