require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes');

const app = express();
const mongooseConnectionString = process.env.MONGOOSE_CONNECTION_STRING;

mongoose.connect(mongooseConnectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

app.use(cors());
app.use(express.json());
app.use(routes);

const port = process.env.PORT || 3333;

app.listen(port, () => 
  console.log(`The server is running on port ${port}! To stop the process, please press CTRL + C`));
