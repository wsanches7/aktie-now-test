const mongoose = require('mongoose');

const BookSchema = new mongoose.Schema({
  name: String,
  author: String,
  abstract: String,
  comments: [String],
});

module.exports = mongoose.model('Book', BookSchema);
