const router = require('express').Router();
const BooksController = require('./controllers/BooksController');

router.get('/books', BooksController.list);
router.post('/books', BooksController.store);
router.get('/books/:id', BooksController.show);
router.put('/books/:id', BooksController.update);
router.delete('/books/:id', BooksController.remove);

module.exports = router;
