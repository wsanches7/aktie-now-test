import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button } from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import "./styles.css";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    right: 0
  },
  error: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

let controlValues = true;

const updateStateWithValues = async (
  values,
  setName,
  setAuthor,
  setAbstract
) => {
  const { name, author, abstract } = await values;
  if (controlValues) {
    setName(name);
    setAuthor(author);
    setAbstract(abstract);
    controlValues = false;
  }
};

const FormBook = props => {
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [abstract, setAbstract] = useState("");
  const [error, setError] = useState("");

  if (props.values) {
    updateStateWithValues(props.values, setName, setAuthor, setAbstract);
  }

  const classes = useStyles();

  return (
    <form
      onSubmit={event =>
        props.handleSubmit(event, setError, name, author, abstract, classes)
      }
    >
      <div id="show-error">{error}</div>
      <TextField
        id="name-input"
        value={name}
        label="Nome"
        name="name"
        className="input"
        variant="outlined"
        style={{ width: "49%", marginRight: "1%" }}
        onChange={event => setName(event.target.value)}
      />
      <TextField
        id="author-input"
        value={author}
        label="Autor"
        name="author"
        className="input"
        variant="outlined"
        style={{ width: "49%", marginLeft: "1%" }}
        onChange={event => setAuthor(event.target.value)}
      />
      <TextField
        className="input-abstract"
        value={abstract}
        placeholder="Insira aqui o resumo do livro"
        multiline={true}
        rows={4}
        fullWidth={true}
        onChange={event => setAbstract(event.target.value)}
      />
      <div className="row-button">
        <Button
          variant="contained"
          onClick={() => props.history.push("/")}
          size="large"
          className={classes.button}
          startIcon={<ArrowBackIcon />}
        >
          Voltar
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          size="large"
          className={classes.button}
          startIcon={<SaveIcon />}
        >
          Gravar
        </Button>
      </div>
    </form>
  );
};

export default FormBook;
