import React from "react";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles } from "@material-ui/core/styles";
import VisibilityIcon from '@material-ui/icons/Visibility';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import api from '../../services/api';

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(1)
    }
  }
}));

const viewAction = (id, history) => {
  history.push(`/view/${id}`);
}

const updateAction = (id, history) => {
  history.push(`/update/${id}`);
}

const deleteAction = async (id, updateDataGridState) => {
  await api.delete(`/books/${id}`);
  updateDataGridState([]);
}

const ActionsGrid = props => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <ButtonGroup size="small" aria-label="small outlined button group">
        <Button startIcon={<VisibilityIcon />} onClick={() => viewAction(props.itemId, props.history)}>Visualizar</Button>
        <Button color="primary" startIcon={<SaveIcon />} onClick={() => updateAction(props.itemId, props.history)}>Editar</Button>
        <Button color="secondary" startIcon={<DeleteIcon />} onClick={() => deleteAction(props.itemId, props.updateDataGridState)}>Deletar</Button>
      </ButtonGroup>

    </div>
  );
};

export default ActionsGrid;
