import React from "react";
import api from '../../services/api';
import Alert from '@material-ui/lab/Alert';
import FormBook from '../FormBook';

const FormNewBook = (props) => {

  const handleSubmit = async (event, setError, name, author, abstract, classes) => {
    event.preventDefault();
  
    try {
      await api.post('/books', {
        name, author, abstract
      });
    } catch (error) {
      if('Request failed with status code 409')
        error.message = `Já existe um livro cadastrado com o nome ${name} do mesmo author.`;
  
      setError(<Alert severity="error" className={classes.button} onClose={() => {setError('')}}>{error.message}</Alert>);
    }
  
    props.history.push('/');
  }

  return (<FormBook handleSubmit={handleSubmit} history={props.history} />);
};

export default FormNewBook;
