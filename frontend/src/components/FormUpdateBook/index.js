import React from "react";
import api from '../../services/api';
import Alert from '@material-ui/lab/Alert';
import FormBook from '../FormBook';

const getBookData = async (id) => {
  const response = await api.get(`/books/${id}`);
  return response.data;
};

const FormUpdateBook = (props) => {

  const handleSubmit = async (event, setError, name, author, abstract, classes) => {
    event.preventDefault();
  
    try {
      await api.put(`/books/${props.id}`, {
        name, author, abstract
      });
    } catch (error) {
      setError(<Alert severity="error" className={classes.button} onClose={() => {setError('')}}>{error.message}</Alert>);
    }
  
    props.history.push('/');
  }

  const values = getBookData(props.id);

  return (<FormBook handleSubmit={handleSubmit} history={props.history} values={values} />);
};

export default FormUpdateBook;
