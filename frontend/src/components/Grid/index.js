import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper
} from "@material-ui/core";

import ActionsGrid from "../ActionsGrid";

import "./styles.css";

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledActionsTableCell = withStyles(theme => ({
  body: {
    fontSize: 14,
    backgroundColor: "#f7f7f7",
    width: 300
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700
  }
});

const Grid = props => {
  const classes = useStyles();

  if(!props.data.length)
    return (<></>);

  const tableHeaders = Object.getOwnPropertyNames(props.data[0]);

  return (
    <TableContainer className="grid" component={Paper}>
      <Table className={classes.table} aria-label="grid">
        <TableHead>
          <TableRow>
            {tableHeaders.map((text, index) => (
              <StyledTableCell align="center" key={index} >
                {text === "_id" ? "Actions" : `${text.charAt(0).toUpperCase()}${text.slice(1)}`}
              </StyledTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.data.map((row, index) => (
            <StyledTableRow key={index}>
              {tableHeaders.map((text, i) =>
                text === "_id" ? (
                  <StyledActionsTableCell align="center" key={i}>
                    <ActionsGrid itemId={row[text]} updateDataGridState={props.updateDataGridState} history={props.history} />
                  </StyledActionsTableCell>
                ) : (
                  <StyledTableCell align="center" key={i}>{row[text]}</StyledTableCell>
                )
              )}
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Grid;
