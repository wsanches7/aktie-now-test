import React, { useState, useEffect } from "react";
import Grid from "../Grid";
import api from "../../services/api";

const limitAbrastract = (text) => {
  if(!text || text.length < 100)
    return text;

  return `${text.slice(0, 100)}...`;
};

const BooksGrid = ({ history }) => {
  const [dataGrid, setDataGrid] = useState([]);

  useEffect(() => {
    const populateGrid = async () => {
      const result = await api.get("/books");

      if (!dataGrid.length){
        const data = result.data.map(item => ({
          name: item.name,
          author: item.author,
          abstract: limitAbrastract(item.abstract),
          _id: item._id
        }));
        setDataGrid(data);
      }
    };

    populateGrid();
  });

  return (
    <div>
      <Grid data={dataGrid} updateDataGridState={setDataGrid} history={history} />
    </div>
  );
};

export default BooksGrid;
