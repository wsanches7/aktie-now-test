import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import CreateIcon from '@material-ui/icons/Create';

import BooksGrid from '../../components/BooksGrid';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    left: 0,
    marginTop: '20px'
  }
}));

function Home({ history }) {
  const classes = useStyles();

  return (
    <>
      <Button
          variant="contained"
          onClick={() => history.push('/new')}
          size="large"
          color="primary"
          className={classes.button}
          startIcon={<CreateIcon />}
        >
          Novo livro
        </Button>
      <BooksGrid history={history} />
    </>
  );
}

export default Home;
