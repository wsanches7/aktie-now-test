import React from 'react';

import FormUpdateBook from '../../components/FormUpdateBook';

function Update({ history, match }) {

  return (
    <>
      <FormUpdateBook history={history} id={match.params.id} />
    </>
  );
}

export default Update;
