import React from 'react';

import FormNewBook from '../../components/FormNewBook';

function New({ history }) {
  return (
    <>
      <FormNewBook history={history} />
    </>
  );
}

export default New;
