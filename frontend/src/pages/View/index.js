import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import CircularProgress from "@material-ui/core/CircularProgress";

import api from "../../services/api";
import "./styles.css";

const prepareData = async (id, setData, setOk, ok) => {
  if (ok) return;

  const response = await api.get(`/books/${id}`);
  setData(response.data);
  setOk(true);
  return "ok";
};

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    left: 0,
    marginTop: "20px"
  }
}));

const View = ({ history, match }) => {
  const [data, setData] = useState({});
  const [ok, setOk] = useState(false);
  const classes = useStyles();

  prepareData(match.params.id, setData, setOk, ok);
  return (
    <>
      <div className="row">
        <Button
          variant="contained"
          onClick={() => history.push("/")}
          size="large"
          className={classes.button}
          startIcon={<ArrowBackIcon />}
        >
          Voltar
        </Button>
      </div>
      <div className="row-content">
        {!ok ? (
          <CircularProgress />
        ) : (
          <div>
            <h1>
              <strong>Nome:</strong> {data.name}
            </h1>
            <br />
            <h2>
              <strong>Autor:</strong> {data.author}
            </h2>
            <br />
            <h2>
              <strong>Resumo</strong>
            </h2>
            <p>{data.abstract}</p>
            <br />
            <hr />
            <br />
            <div className="comments">
              {data.comments.length && (
                <>
                  <h1>Comentários</h1>
                  <ul>
                    {data.comments.map((comment, index) => (
                      <li key={index}>{comment}</li>
                    ))}
                  </ul>
                </>
              )}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default View;
