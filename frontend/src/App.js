import React from 'react';
import Container from '@material-ui/core/Container';
import './App.css';

import Header from './components/Header';
import Routes from './routes';

function App() {
  return (
    <div className="App">
      <Header title='Cadastro de livros' />
      <Container>
        <Routes />
      </Container>
    </div>
  );
}

export default App;
