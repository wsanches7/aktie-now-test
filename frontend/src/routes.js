import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import New from './pages/New';
import Update from './pages/Update';
import View from './pages/View';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/new" component={New} />
        <Route path="/update/:id" component={Update} />
        <Route path="/view/:id" component={View} />
      </Switch>
    </BrowserRouter>
  );
}