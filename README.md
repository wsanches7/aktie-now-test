# Aktie Now Test

To run this project in dev you need start the server first.

```
cd backend
npm run dev
```

Or to run in production environments

```
cd backend
npm start
```

To run the front-end of this project you need to config the endpoint of API's at the line 4 in the follow file.
```src/services/api.js```

After config the endpoint, you can start the front-end.
```
cd frontend
npm start
```

You can see the project running [in this link](https://aktie-now-test-front.herokuapp.com/).

If you have any questions feel free to contact us by the email bellow.
sanches.welton@gmail.com